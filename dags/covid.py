import json
import redis
import requests


_redis = redis.Redis(host='localhost', port=6379, db=0)


class CovidCases:

    def __init__(self, url):
        self._url = url

    def get_covid_data_summary(self):
        response = requests.get(self._url)
        self._data_summary = response.json()
        _redis.set('covid_data_summary',
                   json.dumps(self._data_summary))

    def get_most_effeted_and_least_effected_countries(self):
        self._data_summary = json.loads(_redis.get('covid_data_summary'))
        if self._data_summary:
            countries = sorted(
                self._data_summary['Countries'], key=lambda k: k['TotalConfirmed'])
            self._most_effected_countries = countries[-10:][::-1]
            self._least_effected_countries = countries[:10]
            _redis.set('most_effected_countries',
                       json.dumps(self._most_effected_countries))
            _redis.set('least_effected_countries',
                       json.dumps(self._least_effected_countries))
        else:
            pass

    def get_countries_with_high_death_rate(self):
        self._data_summary = json.loads(_redis.get('covid_data_summary'))
        if self._data_summary:
            for country in self._data_summary['Countries']:
                if country['TotalConfirmed']:
                    country['DeathRate'] = (
                        country['TotalDeaths']/country['TotalConfirmed']) * 100
                else:
                    country['DeathRate'] = 0
            countries = sorted(
                self._data_summary['Countries'], key=lambda k: k['DeathRate'], reverse=True)
            self._countries_with_high_death_rate = countries[:10]
            _redis.set('countries_with_high_death_rate',
                       json.dumps(self._countries_with_high_death_rate))
        else:
            pass
