import requests

from datetime import timedelta
# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG
# Operators; we need this to operate!
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago
# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization

from covid import CovidCases

default_args = {
    'owner': 'aswincv',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=60),
}

dag = DAG(
    'test_covid',
    default_args=default_args,
    description='A DAG to find covid updates',
    schedule_interval=timedelta(days=1),
)

# url should be moved to variable
url = 'https://api.covid19api.com/summary'
obj = CovidCases(url)


t1 = PythonOperator(
    task_id='get_data_summary',
    python_callable=obj.get_covid_data_summary,
    dag=dag,
)

t2 = PythonOperator(
    task_id='get_most_effeted_and_least_effected_countries',
    python_callable=obj.get_most_effeted_and_least_effected_countries,
    dag=dag,
)

t3 = PythonOperator(
    task_id='get_countries_with_high_death_rate',
    python_callable=obj.get_countries_with_high_death_rate,
    dag=dag,
)

dag.doc_md = __doc__


t1 >> t2
t1 >> t3
