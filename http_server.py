import json
import redis

from flask import Flask, request


_redis = redis.Redis(host='localhost', port=6379, db=0)
app = Flask(__name__)


@app.route('/most_effected_countries', methods=['GET'])
def get_most_effeted_countries():
    resp = _redis.get('most_effected_countries')
    print(resp)
    return resp


@app.route('/least_effected_countries', methods=['GET'])
def get_least_effected_countries():
    resp = _redis.get('least_effected_countries')
    print(resp)
    return resp


@app.route('/countries_with_high_death_rate', methods=['GET'])
def get_countries_with_high_death_rate():
    resp = _redis.get('countries_with_high_death_rate')
    print(resp)
    return resp


if __name__ == '__main__':
    app.run()
